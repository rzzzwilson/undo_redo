#!/usr/bin/env python

"""
Test the Undo/Redo class using commands on the command line..

The commands allowed are:

    state <value>    changes the state to <value>, saved
    state            prints the current state
    dump             shows the state of the undo/redo system
    undo             do an "undo"
    redo             do a "redo"
    quit             end the program
    help             prints this help


Multiple commands may be entered on one line if they are
separated by the ";" character, like this:

    undo; state
"""

import sys
from copy import deepcopy
import getopt
import traceback
import readline

from undo_redo import UndoRedo


def get_commands():
    """Get possibly multiple commands, return in a list."""

    try:
        cmd = input("?> ")
    except (EOFError, KeyboardInterrupt):
        print()
        return None

    cmd_list = [c.strip() for c in cmd.split(";")]
    return [c for c in cmd_list if len(c)]  # remove empty commands

def handle_commands(commands, state, undo_redo):
    """Process each command in the 'commands' list.

    Returns the state if processing should continue, else
    returns None meaning a "quit" was found.

    Any problems return True immediately, aborting any
    remaining commands.
    """

    for cmd in commands:
        fields = cmd.split()
        if len(fields) not in (1, 2):
            print("Use 'help' to get help")
            break

        cmd = fields[0].lower()
        arg = fields[1:]

        # examine command and perform action
        if cmd == "quit":
            return None
        elif cmd == "state":
            if arg:
                undo_redo.save(deepcopy(state))
                state = [arg[0], {arg[0]: "test"}]
                print(f"State {state} saved")
            else:
                print(f"Current state is {state}")
        elif cmd == "dump":
            print()
            print(f"Current state: {state}")
            print("-" * 50)
            print(undo_redo.dump())
        elif cmd == "undo":
            if undo_redo.can_undo():
                state = undo_redo.undo(deepcopy(state))
                print(f"After 'undo' current state is {state}")
            else:
                print(f"Can't undo, state remains as {state}")
        elif cmd == "redo":
            if undo_redo.can_redo():
                state = undo_redo.redo(deepcopy(state))
                print(f"After 'redo' current state is {state}")
            else:
                print(f"Can't redo, state remains as {state}")
        elif cmd == "help":
            usage()
        else:
            print("Use 'help' to get help")
            break

    return state

def main():
    """Run the command loop, accept and action commands."""

    # start the undo/redo system
    undo_redo = UndoRedo()
    state = "<initial state>"

    while True:
        commands = get_commands()
        if commands is None:
            return

        state = handle_commands(commands, state, undo_redo)
        if state is None:
            return

def usage(msg=None):
    """Help for the befuddled user."""

    if msg:
        print(f"{'*'*80}\n{msg}\n{'*'*80}")
    print(__doc__)

def excepthook(type, value, tb):
    """Our own handler for uncaught exceptions."""

    msg = f"\n{'=' * 80}"
    msg += "\nUncaught exception:\n"
    msg += "".join(traceback.format_exception(type, value, tb))
    msg += f"{'=' * 80}\n"
    print(msg)    # usually we would log the exception

# plug our handler into the python system
sys.excepthook = excepthook

# parse the CLI params
argv = sys.argv[1:]
try:
    (opts, args) = getopt.getopt(argv, "h", ["help"])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

for (opt, param) in opts:
    if opt in ["-h", "--help"]:
        usage()
        sys.exit(0)

# run the program code
main()
