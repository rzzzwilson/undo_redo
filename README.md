# undo_redo

A class to handle undo/redo in an application.

The *hexed* editor uses this to handle undo/redo.  It has been made its own
project to make it more easily reusable.
