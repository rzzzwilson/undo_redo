#An Undo/Redo class.

##Basic Data Structures

The basic data structures are:

    .undo_list  list of undo objects
    .redo_list  list of redo objects

On a `.save()` call, put the current state on end of `undo_list`, and truncate
the `redo_list`.

On `.undo()` save the current state to the `redo_list`, pop youngest state from
`undo_list` and return it as the new state.

On `.redo()` save the current state to the `undo_list`, pop next redo from
the `redo_list` and return it as the new state.

The application code must implement some way to capture the current state of the
application as an object.  This is the state passed to `save()`.  The state
object must be a deepcopy of the state because we don't want any state changes
in the application being visible in the saved undo/redo states.

# The API

The *state* below is the object that we are saving in the undo/redo system.
This would typically be a sequence of state objects that totally defines the
state of the system using the undo/redo.

**obj = UndoRedo()**

    Create the undo/redo object.

**obj.save(current_state)**

    Save `current_state` to the undo/redo object.

**new_state = obj.undo(current_state)**

    Return UNDO `new_state`, else None if no UNDO possible

**new_state = obj.redo(current_state)**

    Return REDO state, else None if no REDO possible

**bool = obj.can_undo()**

    Return True if UNDO is possible

**bool = obj.can_redo()**

    Return True if REDO is possible

**str = obj.dump()**

    Returns a debug string of the undo/redo object state.

# Example Code

The test suite `test_undo_redo.py` shows how to call the API.
