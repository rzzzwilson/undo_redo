"""
An Undo/Redo class.

The basic data structures are:
    .undo_list  list of undo objects
    .redo_list  list of redo objects

On a .save() call, put the current state onto the end of undo_list, and
truncate the redo_list.

On .undo() save the current state to the redo_list, pop the youngest state
from the undo_list, and return it as the current state.

On .redo() save the current state to the undo_list, pop the next redo state
from the redo_list and return it as the current state.

Methods:
    obj = UndoRedo(state)
        Create the undo/redo object.

    obj.save(state)
        Save 'state' to system

    state = obj.undo()
        Pop UNDO state, return None if no UNDO possible

    state = obj.redo()
        Pop REDO state, return None if no REDO possible

    bool = obj.can_undo()
        Return True if UNDO is possible

    bool = obj.can_redo()
        Return True if REDO is possible

    str = obj.dump()
        Returns a debug string of module state
"""

import sys
from pprint import pformat

class UndoRedo:
    """A class that encapsulates the undo/redo mechanism."""

    def __init__(self):
        """Instantiate the undo/redo object."""

        self.undo_list = []                     # list of undo states
        self.redo_list = []                     # list of redo states

    def save(self, state):
        """Save a new current state.

        state  the new application state

        Any pending redo states are deleted.
        """

        self.undo_list.append(state)
        self.redo_list = []

    def undo(self, state):
        """Do an undo.

        state  the current application state to push to redo_list

        Return the undo state, or None if undo not possible.
        """

        if not self.undo_list:
            return None

        self.redo_list.append(state)
        return self.undo_list.pop()

    def redo(self, state):
        """Do a redo.

        state  the current application state to push to undo_list

        Return any redo state, or None if redo not possible.
        """

        if not self.redo_list:
            return None

        self.undo_list.append(state)
        return self.redo_list.pop()

    def can_undo(self):
        """Can we do an undo?  Return True if so."""

        return bool(self.undo_list)

    def can_redo(self):
        """Can we do a redo?  Return True if so."""

        return bool(self.redo_list)

    def para_indent(self, msg, indent):
        """Indent second and following lines by "indent" spaces.

        Helper function for .dump().
        """

        msg_lines = msg.splitlines()
        result = [msg_lines[0]]         # leave first line alone
        rest = [" "*indent + line for line in msg_lines[1:]]
        result.extend(rest)
        return "\n".join(result)

    def dump(self):
        """Return system state as a string.

        Try to shorten lines.
        """

        result = []
        indent = 12

        if self.undo_list:
            prefix = "   Undo"
            for (i, s) in enumerate(self.undo_list):
                formatted = pformat(s, width=50, compact=True)
                formatted = self.para_indent(formatted, indent)
                result.append(f"{prefix} {i:2d}: {formatted}")
                prefix = " " * len(prefix)      # empty prefix after first entry
        else:
            result.append("   Undo   :")

        result.append("          :=========================")

        if self.redo_list:
            prefix = "   Redo"
            redo_len = len(self.undo_list)
            for s in self.redo_list[::-1]:
                formatted = pformat(s, width=50, compact=True)
                formatted = self.para_indent(formatted, indent)
                result.append(f"{prefix} {redo_len:2d}: {formatted}")
                redo_len += 1
                prefix = " " * len(prefix)      # empty prefix after first entry

        else:
            result.append("   Redo   :")

        # test
        import psutil
        process = psutil.Process()
        mem_used = process.memory_info().rss  # in bytes
        result.append(f"Total memory used: {mem_used/1024/1024:.1f} MiB")

        return "\n".join(result)
