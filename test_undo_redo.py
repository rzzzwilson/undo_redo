#!/usr/bin/env python

"""
Test the Undo/Redo class.
"""

import copy
import unittest
from undo_redo import UndoRedo


class TestEnum(unittest.TestCase):

    def test_null(self):
        """Just instantiate object, 'state' doesn't change."""

        state = [1]
        undo_redo = UndoRedo()
        self.assertEqual(state, [1])            # 'state' shouldn't change
        self.assertFalse(undo_redo.can_redo())  # can't redo
        self.assertFalse(undo_redo.can_undo())  #     or undo

    def test_simple(self):
        """Do simple save/undo until undo empty."""

        # start undo/redo system, save initial state
        undo_redo = UndoRedo()
        state = [1]
        copy_state = copy.deepcopy(state)
        undo_redo.save(copy_state)              # undo|redo: [1] | <empty>
        self.assertTrue(undo_redo.can_undo())   # can undo
        self.assertFalse(undo_redo.can_redo())  # can't redo
        self.assertEqual(state, [1])            # 'state' shouldn't change

        # change state, note change in undo/redo system
        state = 2
        undo_redo.save(state)                   # undo|redo: [1] 2 | <empty>
        self.assertTrue(undo_redo.can_undo())   # now CAN undo
        self.assertFalse(undo_redo.can_redo())  # still can't redo
        self.assertEqual(state, 2)              # 'state' shouldn't change

        # now undo, back to 2 state
        state = undo_redo.undo(3)               # expect 2 state
                                                # undo|redo: [1] | 3
        self.assertTrue(undo_redo.can_undo())   # CAN undo
        self.assertTrue(undo_redo.can_redo())   # and we now CAN redo
        self.assertEqual(state, 2)              # check we actually have the 2 state

        # now undo, back to initial [1] state
        state = undo_redo.undo(4)               # expect [1] state
                                                # undo|redo: <empty> | 4 3
        self.assertFalse(undo_redo.can_undo())  # CAN't undo
        self.assertTrue(undo_redo.can_redo())   # and we CAN redo
        self.assertEqual(state, [1])            # check we actually have the 2 state

        # now try to undo back past beginning of history
        state = undo_redo.undo(5)               # undo|redo: <empty> | 5 4 3
        self.assertFalse(undo_redo.can_undo())  # can't undo
        self.assertTrue(undo_redo.can_redo())   # still can redo
        self.assertEqual(state, None)           # we got "None" for initial state

        # try again to rewind back past beginning, same results as above
        state = undo_redo.undo(6)               # undo|redo: <empty> | 6 5 4 3
        self.assertFalse(undo_redo.can_undo())
        self.assertTrue(undo_redo.can_redo())
        self.assertEqual(state, None)

    def test_undo_redo(self):
        """Do save/undo/redo and bounce off undo/redo 'ends'."""

        undo_redo = UndoRedo()                  # undo|redo: <empty> | <empty>

        state = 1
        undo_redo.save(state)                   # undo|redo: 1 | <empty>

        state = 2
        undo_redo.save(state)                   # change to 2, save
                                                # undo|redo: 1 2 | <empty>

        state = 3
        undo_redo.save(state)                   # change to 3, save
                                                # undo|redo: 1 2 3 | <empty>

        state = 4
        state = undo_redo.undo(4)               # undo
                                                # undo|redo: 1 2 | 4 
        self.assertEqual(state, 3)              # expect 3 back

        state = 5
        state = undo_redo.undo(state)           # undo
                                                # undo|redo: 1 | 5 4 
        self.assertEqual(state, 2)              # expect 2 back

        state = 6
        state = undo_redo.undo(6)               # undo
                                                # undo|redo: <empty> | 6 5 4 
        self.assertEqual(state, 1)              # expect 1 back

        state = 7
        state = undo_redo.undo(state)           # undo, no undo, returns None
                                                # undo|redo: <empty> | 6 5 4 
        self.assertEqual(state, None)           # expect None back, no undo

        state = 8
        state = undo_redo.redo(8)               # redo
                                                # undo|redo: <empty> | 5 4
        self.assertEqual(state, 6)              # expect 6 back

        state = 9
        state = undo_redo.redo(state)           # redo
                                                # undo|redo: <empty> | 4
        self.assertEqual(state, 5)              # expect 5 back

        state = 10
        state = undo_redo.redo(10)              # redo
                                                # undo|redo: <empty> | <empty>
        self.assertEqual(state, 4)              # expect 4 back

        state = 11
        state = undo_redo.redo(state)           # redo
                                                # undo|redo: <empty> | <empty>
        self.assertEqual(state, None)           # expect None back

    def test_complex(self):
        """Use complex object as "state" object."""

        state = [0, 1, 2]
        copy_state = copy.deepcopy(state)
        undo_redo = UndoRedo()
        undo_redo.save(copy_state)
        self.assertEqual(state, copy_state)     # 'state' shouldn't change

        state[1] = 42
        copy_state = copy.deepcopy(state)
        undo_redo.save(copy_state)
        self.assertEqual(state, copy_state)     # 'state' shouldn't change

        state = undo_redo.undo(None)            # pop back to first state
        state = undo_redo.undo(None)
        self.assertEqual(state, [0, 1, 2])

    def test_undo_redo(self):
        """Do save/undo/redo and bounce off undo/redo 'ends'."""

        undo_redo = UndoRedo()
        state = 1
        undo_redo.save(state)                   # undo|redo: 1 | <empty>

        state = 2
        undo_redo.save(state)                   # change to 2, save
                                                # undo|redo: 1 2 | <empty>

        state = 3
        undo_redo.save(state)                   # change to 3, save
                                                # undo|redo: 1 2 3 | <empty>

        state = 4
        undo_redo.save(state)                   # change to 4, save
                                                # undo|redo: 1 2 3 4 | <empty>

        state = 5
        undo_redo.save(state)                   # change to 5, save
                                                # undo|redo: 1 2 3 4 5 | <empty>

        state = 6                               # chage to 6
        state = undo_redo.undo(state)           # undo, back to 5
        state = undo_redo.undo(state)           # undo, back to 4
        self.assertEqual(state, 4)              # expect 3 back


unittest.main()

